// fuzzy variables
var dexterity, inteligence, courage, strength, patriotism, charisma;

function leader(){
	return charisma > 0.75;
}

function fearless(){
	return courage > 0.75;
}

function fearful(){
	return courage < 0.5;
}

function intelligent(){
	return inteligence > 0.5;
}

function skillfull(){
	return dexterity > 0.5;
}

function fanatic(){
	return patriotism > 0.75;
}

function charismatic(){
	return charisma > 0.5;
}

function ninja(){
	return dexterity > 0.75;
}

function barbarian(){
	return strength > 0.5 && inteligence < 0.5;
}

function decideRole(){
	var role;
	
	if(leader()){
		if(skillfull()){
			role = roles.archerLeader;
		}
		else{
			role = roles.knightLeader;
		}
	}
	else if(fearless()){
		if(fanatic()){
			role = roles.suicider;
		}
		else if(charismatic()){
			role = roles.assassin;
		}
		else{
			role = roles.saboteur;
		}
	}
	else if(fearful()){
		if(intelligent()){
			role = roles.engeneer;
		}
		else if(skillfull()){
			role = roles.archer;
		}
		else{
			role = roles.soldier;
		}
	}
	else{
		if(barbarian()){
			role = roles.barbarian;
		}
		else if(ninja()){
			role = roles.ninja;
		}
		else{
			role = roles.soldier;
		}
	}
	
	return role;
}

//questions
var questions = [
	{
		text: "Se você estivesse cavalgando em um bosque e avistasse um urso faminto. O que faria?",
		answers: [
			{
				text: "Cavalgaria para longe, uma vez que o urso é mais lento.",
				action: function(){
					courage -= 0.25;
				}
			},
			{
				text: "Enfrentaria o urso com minha espada.",
				action: function(){
					courage += 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Caso estivesse perdido na selva, com muita fome, e avistasse uma árvore alta, cheia de frutos em sua copa e também caídos no chão. Em que lugar buscaria os frutos?",
		answers: [
			{
				text: "Usaria algum objeto (pedra ou lança por exemplo) para colher frutos direto da árvore.",
				action: function(){
					dexterity += 0.25;
				}
			},
			{
				text: "Procuraria por frutos maduros que acabram de cair no chão.",
				action: function(){
					dexterity -= 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Voce deixaria de lado uma vida estável para lutar pelo bem de sua nação caso ela estivesse em perigo?",
		answers: [
			{
				text: "Sim.",
				action: function(){
					patriotism += 0.25;
				}
			},
			{
				text: "Não.",
				action: function(){
					patriotism -= 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Supondo que alguém lhe pedisse para buscar lenhas. Qual das opções de transporte você escolheria?",
		answers: [
			{
				text: "Fazer várias viagens rápidas com menos peso.",
				action: function(){
					strength -= 0.25;
				}
			},
			{
				text: "Fazer poucas viagens lentas com mais peso.",
				action: function(){
					strength += 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Caso estevisse em um péssimo dia e alguém lhe cumprimentasse alegremente. Você:",
		answers: [
			{
				text: "Cumprimentaria proporcionalmente.",
				action: function(){
					charisma += 0.25;
				}
			},
			{
				text: "Não conseguiria demonstraria a mesma alegria.",
				action: function(){
					charisma -= 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Supondo que precise resolver alguns cálculos matemáticos. Você:",
		answers: [
			{
				text: "Pagaria para algum conhecido fazê-los por mim.",
				action: function(){
					inteligence -= 0.25;
				}
			},
			{
				text: "Resolveria eu mesmo.",
				action: function(){
					inteligence += 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Se alguem lhe oferecer dois trabalhos: O primeiro seria empilhar sacos de areia e o segundo seria empilhar o dobro de sacos de feno. Ambos possuem a mesma recompensa. Qual escolheria?",
		answers: [
			{
				text: "Sacos de areia.",
				action: function(){
					strength += 0.25;
				}
			},
			{
				text: "Sacos de feno",
				action: function(){
					strength -= 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Em uma mesa há dois potes. O primeiro é transparente e possui um anel de ouro. O Segundo é escuro, porém sabe-se que há um anel igual ao primeiro, além de algo que pode ser tanto bom, quanto ruim. Qual dos potes você escolheria?",
		answers: [
			{
				text: "Primeiro.",
				action: function(){
					courage -= 0.25;
				}
			},
			{
				text: "Segundo.",
				action: function(){
					courage += 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Caso possuisse muito dinheiro e precisasse urgentemente comprar uma ferramenta. Você:",
		answers: [
			{
				text: "Pagaria o valor padrão oferecido pelo vendedor.",
				action: function(){
					charisma -= 0.25;
				}
			},
			{
				text: "Tentaria negociar o valor.",
				action: function(){
					charisma += 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Se lhe oferecessem o mesmo salário para estudar ou trabalhar, qual opção escolheria?",
		answers: [
			{
				text: "Trabalhar.",
				action: function(){
					inteligence -= 0.25;
				}
			},
			{
				text: "Estudar.",
				action: function(){
					inteligence += 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Pelo bem do seu país, voce deixaria algum familiar próximo ir à guerra voluntariamente?",
		answers: [
			{
				text: "Sim.",
				action: function(){
					patriotism += 0.25;
				}
			},
			{
				text: "Não.",
				action: function(){
					patriotism -= 0.25;
				}
			}
		],
		answered: false
	},
	{
		text: "Qual dessas opções você usaria para atravessar um riacho:",
		answers: [
			{
				text: "Canoa (menos rápido, mais seguro).",
				action: function(){
					dexterity -= 0.25;
				}
			},
			{
				text: "Cipó (mais rapido, menos seguro).",
				action: function(){
					dexterity += 0.25;
				}
			}
		],
		answered: false
	}
	
];

var roles = {
	saboteur: {name: "Sabotador", description: "Seu objeto é infiltrar-se nas dependencias inimigas e implantar explosivos no local."},
	suicider: {name: "Suicida", description: "Em nome da pátria, seu objetivo é infiltrar-se no exercito inimigo e suicidar-se com explosivos amarrados ao corpo"},
	assassin: {name: "Assassino", description: "Seu objetivo é se passar por um membro do exercito inimigo e assassinar um líder das tropas"},
	archerLeader: {name: "Arqueiro líder do batalhão", description: "Seu objetivo é liderar um batalhão do exercito, armado de arco e flecha e montado em um cavalo"},
	knightLeader: {name: "Cavaleiro líder do batalhão", description: "Seu objetivo é liderar um batalhão do exercito, armado com uma lança e montado em um cavalo"},
	archer: {name: "Arqueiro", description: "Seu objetivo é permanecer em um pelotão recuado, atacando os inimigos com arco e flecha"},
	soldier: {name: "Soldado", description: "Seu objetivo é ser membro de um batalhão, armado de espada e escudo"},
	barbarian: {name: "Bárbaro", description: "Seu objeto é lutar na linha de frente com uma machado feito para esmagar os inimigos"},
	engeneer: {name: "Engenheiro", description: "Seu trabalho é manipular ferramentas de guerra como catapultas e trebuchets"},
	ninja: {name: "Ninja", description: "Seu trabalho é munir-se de uma adaga em cada mão e assassinar o maior número de inimigos no menor espaço de tempo"},
	
};

function resetVariables(){
	dexterity = inteligence = courage = strength = patriotism = charisma = 0.5;
	
	questions.forEach(function(e){
		e.answered = false;
	});
}

function startTest(){
	resetVariables();
	
	//display very first question
	if(questions.length > 0){
		applyQuestion(0);
	}
}

$(document).ready(function(){
	$("#againBtn").click(function(){
		startTest();
		
		fadeOutResult(fadeInQuestion);
	});
	
	startTest();
});

function applyQuestion(index){
	console.log("ÉOQ? " + questions[index].answered);
	var questionElement = $("#question");
	
	//change question text
	questionElement.text(questions[index].text);
	
	//set answers text and callback
	var answerA = $("#answerA");
	answerA.text(questions[index].answers[0].text);
	answerA.unbind("click");
	answerA.click(function(){
		if(!questions[index].answered){
			onAnswerSelected(questions[index].answers[0]);
			questions[index].answered = true;
		}
	});
	
	var answerB = $("#answerB");
	answerB.text(questions[index].answers[1].text);
	answerB.unbind("click");
	answerB.click(function(){
		if(!questions[index].answered){
			onAnswerSelected(questions[index].answers[1]);
			questions[index].answered = true;
		}
	});
	
	function onAnswerSelected(answer){
		//perform action according to the chosen answer
		answer.action();
		
		//next question
		if(index < questions.length - 1){
			fadeOutQuestion(function(){
				applyQuestion(++index);
				fadeInQuestion();
			});
		}
		else{
			fadeOutQuestion(showResult);
		}
		
		
	}
}

function showResult(){
	var result = decideRole();
	
	$("#resultName").text("Sua função é: " + result.name);
	$("#resultDescription").text(result.description);
	
	fadeInResult();
}

function fadeOutQuestion(onAnimationFinished){
	$("#questionContainer").fadeOut(380, onAnimationFinished);
	
}

function fadeInQuestion(){
	$("#questionContainer").fadeIn(380);
}

function fadeOutResult(onAnimationFinished){
	$("#resultContainer").fadeOut(380, onAnimationFinished);
	
}

function fadeInResult(){
	$("#resultContainer").fadeIn(380);
}